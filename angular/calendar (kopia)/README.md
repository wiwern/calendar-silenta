# Calendar

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.2.

## Installation

Clone repository by typing password "Miserere95". Install Calendar by type `npm install`.  Application will automatically install on your computer and will be ready to develop.

## Development server

Go to folder /src and run `ng serve --open` for a dev server. It can take a few minutes. Due an installation angular can take a few questions about styles, sass, using a different port, but I doesn't affect on installation. The browser automatically will opoen to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.



