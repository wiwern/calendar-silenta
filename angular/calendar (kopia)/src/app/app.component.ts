import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title: string = 'Calendar Silenta';
  option: string = 'week';
  isShowingWelcome = true;
  item: string = '';
  week = '11-17.10.2021';

  constructor() {

  }

  public switchComponent(component: string): void {
    this.option = component;
    this.isShowingWelcome = false;
  }




}
