import { Component, OnInit } from '@angular/core';
import {of} from "rxjs";


@Component({
  selector: 'app-week',
  templateUrl: './week.component.html',
  styleUrls: ['./week.component.scss']
})

export class WeekComponent implements OnInit {
  birthday: Date = new Date(1988, 1, 28);
  hours: string[];
  hoursQuantity: 16;
  source = of(1, 2, 3, 4, 5);
  events: any[];
  days: string[];
  constructor() { }


  ngOnInit(): void {
    this.hours = ['9', '9:30', '10:00', '10:30', '11:00', '11:30', '12:00', '12:30', '13:00', '13:30'];
    this.days = ['godzina', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota', 'Niedziela'];
    this.events = [
      {
        hour: '10:00',
        nameAndSurname: 'Agnieszka Bączkowska',
        timeDuration: '10:00- 10:30',
        place: 'Warszawa Powązki',
        funeralType: 'trumna',
        office: 'Świętowice',
        day: 'Środa'
      },
      {
        hour: '9:30',
        nameAndSurname: 'Jan Kowalski',
        timeDuration: '9:30- 10:30',
        place: 'Warszawa Powązki',
        funeralType: 'trumna',
        office: 'Świętowice',
        day: 'Czwartek'
      }
    ];
    this.source.subscribe(val  => console.log(val));

  }

}
