import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
    selector: 'new-event-dialog',
    templateUrl: 'NewEventDialog.html',
  })
  export class NewEventDialog {
  
    event;
    hour;

    constructor(
      public dialogRef: MatDialogRef<NewEventDialog>,
      @Inject(MAT_DIALOG_DATA) public data) {}
  
    submit(): void {
        console.log(this.event, this.hour);
        const result = {
            event:this.event, 
            hour:this.hour}
      this.dialogRef.close(result);
    }
  
  }