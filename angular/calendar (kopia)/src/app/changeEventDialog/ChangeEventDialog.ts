import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'change-item-dialog',
    templateUrl: 'changeEventDialog.html',
  })
  export class ChangeEventDialog {
  
    public event: string;
    public hour: number;

    constructor(
      public dialogRef: MatDialogRef<ChangeEventDialog>,
      @Inject(MAT_DIALOG_DATA) public data) {
        //this.item = data;
        this.event = data.event;
        this.hour = data.hour;
      }
  
    public submit(): void {
      const event = {event: this.event, hour: this.hour}
      this.dialogRef.close(event);
    }
  
  }