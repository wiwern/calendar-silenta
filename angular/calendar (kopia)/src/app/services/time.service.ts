import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class TimeService {
  public today: Date = new Date();
  public day;
  public year;
  public month;
  public monthName;

  public days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  public months =
    ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];


  constructor( private cookieService: CookieService) {
    this.day = this.today.getUTCDate();
    this.year = this.today.getUTCFullYear();
    this.month = this.today.getMonth();
    this.monthName = this.months[this.today.getMonth()];
  }


}
