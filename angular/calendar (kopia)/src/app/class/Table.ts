
export class TableBasicExample {
  displayedColumns: string[];
  dataSource: any;

  constructor(displayedColumns: string[], dataSource: any) {
    this.displayedColumns = displayedColumns;
    this.dataSource = dataSource;
  }
}
